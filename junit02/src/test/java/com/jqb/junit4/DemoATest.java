package com.jqb.junit4;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class DemoATest {

	private DemoA demoA = null;

	@BeforeClass
	public static void globalInit() {
		System.out.println("globalInit");
		System.out.println("-----------------");
	}

	@AfterClass
	public static void globalDestroy() {
		System.out.println("globalDestroy");
	}

	@Before
	public void init() {
		demoA = new DemoA();
		System.out.println("before");
	}

	@After
	public void destroy() {
		System.out.println("after");
		System.out.println("-----------------");
	}

	@Test
	public void testAdd() {
		int result = demoA.add(1, 0);
		Assert.assertEquals(1, result);
	}

	@Test
	public void testSubtract() {
		int result = demoA.subtract(1, 0);
		Assert.assertEquals(1, result);
	}

	@Test
	public void testMultply() {
		int result = demoA.multiply(1, 0);
		Assert.assertEquals(0, result);
	}

	@Test(timeout = 900)
	public void testDivde1() {
		try {
			Thread.sleep(800);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		int result = 0;
		try {
			result = demoA.divide(1, 1);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("测试失败");
		}
		Assert.assertEquals(1, result);
	}

	@Test(expected = Exception.class)
	public void testDivde2() throws Exception {
		demoA.divide(1, 0);
	}

}
