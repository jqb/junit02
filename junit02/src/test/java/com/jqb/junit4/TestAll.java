package com.jqb.junit4;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ DemoATest.class, DemoBTest.class, ParametersTest.class })
public class TestAll {

}
