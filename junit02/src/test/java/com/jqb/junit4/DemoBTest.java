package com.jqb.junit4;

import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class DemoBTest {
	
	private DemoB demoB = null;
	
	@Before
	public void setUp() throws Exception {
		demoB = new DemoB();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetMaxOfArray1() {
		int[] array = { 1, 2, 9, 4, 5, 6 };
		int max = 0;
		try {
			max = demoB.getMaxOfArray(array);
		} catch (Exception e) {
			e.printStackTrace();
			fail("测试失败");
		}
		Assert.assertEquals(9, max);
	}
	
	@Test(expected=Exception.class)
	public void testGetMaxOfArray2() throws Exception {
		int[] array= null;
		demoB.getMaxOfArray(array);
	}
	
	@Ignore("wait for testing")
	@Test(expected=Exception.class)
	public void testGetMaxOfArray3() throws Exception {
		int[] array= {};
		demoB.getMaxOfArray(array);
	}

}
