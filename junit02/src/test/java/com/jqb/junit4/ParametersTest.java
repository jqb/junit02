package com.jqb.junit4;

import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/***
 * 使用参数化运行器
 * 使用这种方法适用于数据量大的情况
 * 想这个例子如果不用这种方法,我们就要写3个测试用例,很不方便
 * 步骤：
 * 1.提供带参构造
 * 2.提供@Parameters方法,构造测试数据(符合构造函数参数类型和顺序)
 * 3.提供测试用例
 * @author Dong
 *
 */
@RunWith(Parameterized.class)
public class ParametersTest {
	/***
	 * 期望值
	 */
	private int expected;
	/***
	 * 参数1
	 */
	private int i;
	/***
	 * 参数2
	 */
	private int j;
	
	private DemoA demoA = null;

	public ParametersTest(int expected, int i, int j) {
		this.expected = expected;
		this.i = i;
		this.j = j;
	}
	
	@SuppressWarnings("rawtypes")
	@Parameters
	public static Collection prepareData() {
		/**
		 * 构造二维数组
		 */
		Object[][] obj = { { 1, 3, -2 }, { 3, 4, -1 }, { 5, 1, 4 } };
		/**
		 * 转换成Collection对象
		 */
		return Arrays.asList(obj);
	}
	
	@Before
	public void setUp(){
		demoA = new DemoA();
	}

	@Test
	public void testAdd(){
		Assert.assertEquals(this.expected,demoA.add(this.i,this.j));
	}
	
	@After
	public void tearDown(){
		
	}
}
