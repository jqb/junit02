package com.jqb.junit4;

public class DemoE {
	
	/***
	 * 私有方法
	 * @param a
	 * @param b
	 * @return
	 */
	@SuppressWarnings("unused")
	private int Add(int a, int b) {
		return a + b;
	}
}
