package com.jqb.junit4;

public class DemoB {
	public int getMaxOfArray(int[] array) throws Exception {
		int max = 0;
		/***
		 * 一定要先判断是否为null,不然是异常不会是我们抛出的异常,而是NullPointException
		 * 因为数组如果是null,而你调用length属性就会报错,程序终止(基本功啊基本功)
		 */
		if (array == null  || array.length == 0) {
			throw new Exception("数组不能为空");
		} else {
			for (int i = 0; i < array.length; i++) {
				if (max < array[i]) {
					max = array[i];
				}
			}
		}
		return max;
	}
}
